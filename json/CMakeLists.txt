CMAKE_MINIMUM_REQUIRED(VERSION 3.10)
Project(json)

add_subdirectory(third_party/json)

link_libraries(nlohmann_json::nlohmann_json)

add_executable(${PROJECT_NAME} main.cpp)