#!/bin/bash
if [ ! -d "$(pwd)/build" ]; then
    mkdir build
fi 

cd build

if [ ! -d "$(pwd)/build/release" ]; then
    mkdir release
fi 
cd release
cmake -DCMAKE_BUILD_TYPE=Release ../.. 
cmake --build .
cd ..

if [ ! -d "$(pwd)/build/debug" ]; then
    mkdir debug
fi 
cd debug 
cmake -DCMAKE_BUILD_TYPE=Debug ../..
cmake --build .
