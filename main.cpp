#include <iostream>
#include "calculator.hpp"
#include "config.h"
using namespace std;

int main()
{

    int num1, num2;
    calculator obj;
    cout << "calculator version " << calculator_VERSION_MAJOR << "." << calculator_VERSION_MINOR << endl;
    cout << "Enter num1: ";
    cin >> num1;
    cout << "Enter num2: ";
    cin >> num2;
    obj.add(num1, num2);
    obj.sub(num1, num2);
    obj.mul(num1, num2);
    obj.div(num1, num2);

    return 0;
}