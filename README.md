# Cmake Practice 

## Task 1

For this Task **add_executable([name] [source1] [source2 ...])** 

above command will do the work.

## Task 2

Added files from the **gccpracticetask** repository.

Made a Static library using the command **add_library([name] [STATIC | SHARED | MODULE] [source...])**

## Task 3

Linked the library using **target_link_libraries(calculator PUBLIC cal)**

## Task 4

Used the script file for building the cmake file in both release and debug mode

Remain in the project source directory and use **./build_release_debug** It will make both the release and debug directories and there relative files in the respective directories.

## Task 5

To  Setting the warning flags, Use **set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Werror")**
